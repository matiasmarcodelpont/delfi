window.addEventListener("load", function loadDocumen() {
  document.querySelector("main h1").classList.add("centered");
});

let timeoutId = null;
const audio = new Audio("./assets/bark.wav");

document
  .querySelector("main")
  .addEventListener("click", function clickImage(event) {
    timeoutId && clearTimeout(timeoutId);

    document.querySelector("main > img#puppy").src = "./assets/puppy-gif.webp";
    document.querySelector("main > img#vol-icon").src = "./assets/volume.png";

    timeoutId = setTimeout(() => {
      document.querySelector("main > img#puppy").src =
        "./assets/puppy-img.webp";
      document.querySelector("main > img#vol-icon").src = "./assets/mute.png";
    }, 2160);

    audio.play();
  });
